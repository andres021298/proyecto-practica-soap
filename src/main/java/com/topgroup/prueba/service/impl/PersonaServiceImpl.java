package com.topgroup.prueba.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.topgroup.prueba.repository.PersonaRepository;
import com.topgroup.prueba.service.PersonaService;

import topgroup.api.persona.Persona;

@Service
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	PersonaRepository personaRepo;

	@Override
	public Persona buscarPorNombre(String nombre) {
		// TODO Auto-generated method stub
		return personaRepo.getPersonas().get(nombre);
	}

	@Override
	public List<Persona> listarPersonasPorBarrios(String barrio) {
		// TODO Auto-generated method stub
		Map<String, Persona> todasPersonas = personaRepo.getPersonas();
		List<Persona> personasEnBarrio = new ArrayList<>();
		for (Persona persona : todasPersonas.values()) {
			if (persona.getBarrio().equals(barrio)) {
				personasEnBarrio.add(persona);
			}
		}
		return personasEnBarrio;
	}
}
