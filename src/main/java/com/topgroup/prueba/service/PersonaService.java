package com.topgroup.prueba.service;

import java.util.List;

import topgroup.api.persona.Persona;

public interface PersonaService {

	public Persona buscarPorNombre(String nombre);

	public List<Persona> listarPersonasPorBarrios(String barrio);
}
