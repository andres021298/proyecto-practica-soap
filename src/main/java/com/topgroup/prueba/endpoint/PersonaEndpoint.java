package com.topgroup.prueba.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.topgroup.prueba.service.PersonaService;

import topgroup.api.persona.DetallePersonaRequest;
import topgroup.api.persona.DetallePersonaResponse;
import topgroup.api.persona.PersonasPorBarrioRequest;
//import topgroup.api.persona.NombreBarrioRequest;
//import topgroup.api.persona.PersonasPorBarrioResponse;
import topgroup.api.persona.PersonasPorBarrioResponse;

@Endpoint
public class PersonaEndpoint {
	private static final String NAMESPACE_URI = "http://www.topgroup/api/persona";

	@Autowired
	private PersonaService personaService;


	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "DetallePersonaRequest")
	@ResponsePayload
	public DetallePersonaResponse obtenerPersona(@RequestPayload DetallePersonaRequest request) {
		DetallePersonaResponse response = new DetallePersonaResponse();
		response.setPersona(personaService.buscarPorNombre(request.getNombre()));

		return response;
	}


	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "PersonasPorBarrioRequest")
	@ResponsePayload
	public PersonasPorBarrioResponse obtenerPersonasPorBarrio(@RequestPayload PersonasPorBarrioRequest request) {
		PersonasPorBarrioResponse response = new PersonasPorBarrioResponse();
		response.getListaPersonas().addAll(personaService.listarPersonasPorBarrios(request.getNombreBarrio()));
//		response.setPersona(personaService.buscarPorNombre(request.getNombre()));

		return response;
	}
}
