package com.topgroup.prueba.repository;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import topgroup.api.persona.Persona;

@Component
public class PersonaRepository {

	private final Map<String, Persona> personas = new HashMap<>();

	@PostConstruct
	public void initData() {

		Persona persona1 = new Persona();
		persona1.setBarrio("Niza");
		persona1.setEdad(22);
		persona1.setNombre("David Ariza");
		persona1.setTelefono("3005698730");
		personas.put(persona1.getNombre(), persona1);

		Persona persona2 = new Persona();
		persona2.setBarrio("El Bosque");
		persona2.setEdad(24);
		persona2.setNombre("Alexander Acevedo");
		persona2.setTelefono("3012323453");
		personas.put(persona2.getNombre(), persona2);

		Persona persona3 = new Persona();
		persona3.setBarrio("Santa Helena");
		persona3.setEdad(22);
		persona3.setNombre("Diego Navas");
		persona3.setTelefono("3244545678");
		personas.put(persona3.getNombre(), persona3);

		Persona persona4 = new Persona();
		persona4.setBarrio("San Miguel");
		persona4.setEdad(30);
		persona4.setNombre("Jurgen Forero");
		persona4.setTelefono("3213452233");
		personas.put(persona4.getNombre(), persona4);

		Persona persona5 = new Persona();
		persona5.setBarrio("Ciudad Jardin");
		persona5.setEdad(21);
		persona5.setNombre("Joimar Mendoza");
		persona5.setTelefono("3214568898");
		personas.put(persona5.getNombre(), persona5);

		Persona persona6 = new Persona();
		persona6.setBarrio("Alcala");
		persona6.setEdad(28);
		persona6.setNombre("Julian");
		persona6.setTelefono("3142233232");
		personas.put(persona6.getNombre(), persona6);
		
		Persona persona7 = new Persona();
		persona7.setBarrio("Niza");
		persona7.setEdad(30);
		persona7.setNombre("Monica Caceres");
		persona7.setTelefono("3213162266");
		personas.put(persona7.getNombre(), persona7);
	}

	public Map<String, Persona> getPersonas() {
		return this.personas;
	}
}
