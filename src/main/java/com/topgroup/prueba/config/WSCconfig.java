package com.topgroup.prueba.config;

import javax.servlet.Servlet;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@Configuration
@EnableWs
public class WSCconfig extends WsConfigurerAdapter {

	@Bean
	public ServletRegistrationBean<Servlet> messageDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean<Servlet>(servlet, "/service/*");
	}

//    Este sera, junto con /service/ donde se encontrara el servicio para mostrar detalle de persona
	@Bean(name = "detalle-persona")
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema schema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("DetallesPersonaPort");
		wsdl11Definition.setLocationUri("/service/detalle-persona");
		wsdl11Definition.setTargetNamespace("http://www.topgroup/api/persona");
		wsdl11Definition.setSchema(schema);
		return wsdl11Definition;
	}

	
//  Este sera, junto con /service/ donde se encontrara el servicio para mostrar detalle de computador
	@Bean(name = "lista-personas-por-barrio")
	public DefaultWsdl11Definition mostrarListaPersonas(XsdSchema schema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("PersonasPorBarrioPort");
		wsdl11Definition.setLocationUri("/service/lista-personas-por-barrio");
		wsdl11Definition.setTargetNamespace("http://www.topgroup/api/persona");
		wsdl11Definition.setSchema(schema);
		return wsdl11Definition;
	}

//    @Bean(name = "personasPorBarrio")
//    public DefaultWsdl11Definition defaultWsdl11Definition2(XsdSchema countriesSchema) 
//    {
//        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
//        wsdl11Definition.setPortTypeName("PersonaPorBarrioPort");
//        wsdl11Definition.setLocationUri("/service/persona-por-barrio");
//        wsdl11Definition.setTargetNamespace("http://www.topgroup/api/persona");
//        wsdl11Definition.setSchema(countriesSchema);
//        return wsdl11Definition;
//    }

	@Bean
	public XsdSchema countriesSchema() {
		return new SimpleXsdSchema(new ClassPathResource("persona.xsd"));
	}
}
