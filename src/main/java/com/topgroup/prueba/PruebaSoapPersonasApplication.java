package com.topgroup.prueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaSoapPersonasApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaSoapPersonasApplication.class, args);
	}

}
