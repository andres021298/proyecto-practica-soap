//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.10.22 a las 09:47:24 AM COT 
//


package topgroup.api.persona;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the topgroup.api.persona package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: topgroup.api.persona
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DetallePersonaRequest }
     * 
     */
    public DetallePersonaRequest createDetallePersonaRequest() {
        return new DetallePersonaRequest();
    }

    /**
     * Create an instance of {@link PersonasPorBarrioResponse }
     * 
     */
    public PersonasPorBarrioResponse createPersonasPorBarrioResponse() {
        return new PersonasPorBarrioResponse();
    }

    /**
     * Create an instance of {@link Persona }
     * 
     */
    public Persona createPersona() {
        return new Persona();
    }

    /**
     * Create an instance of {@link DetallePersonaResponse }
     * 
     */
    public DetallePersonaResponse createDetallePersonaResponse() {
        return new DetallePersonaResponse();
    }

    /**
     * Create an instance of {@link PersonasPorBarrioRequest }
     * 
     */
    public PersonasPorBarrioRequest createPersonasPorBarrioRequest() {
        return new PersonasPorBarrioRequest();
    }

}
